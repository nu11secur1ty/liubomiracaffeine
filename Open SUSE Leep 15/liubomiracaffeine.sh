#!/usr/bin/bash
###################
# by nu11secur1ty #
###################
# cleanup function with Ctrl+C
cleanup()
{
#   killall -9 firefox-esr
#   killall -9 firefox
   echo -e "\e[00;35m  Clear everything GoodBye =)\e[00m"
       return $?
}

# run if user hits control-c
   control_c()
{
   cleanup
      exit $?
}
# trap keyboard interrupt (control-c)
trap control_c SIGINT

sleep_period=4m
while true; do
  if ps -eo %C --sort -%cpu | head -2 | awk 'NR==2 { exit !($1>10); }'; then
      #xdotool key shift
      xdotool key comma
  fi
  sleep ${sleep_period}
echo "The program still running..."
done;
